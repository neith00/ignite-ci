# Usage

## Prerequisites
https://ignite.readthedocs.io/en/stable/installation/

## ARA Friendly
```
python3 -m ara.setup.callback_plugins &> /dev/null && export ANSIBLE_CALLBACK_PLUGINS="$(python3 -m ara.setup.callback_plugins)"
```

Launch VMs for CI

This example starts 6 VMs locally for CI purpose through ansible.

```console
./run.sh
```

Delete all ignite VMs

```console
ansible-playbook -b bootstrap_ci.yaml  -i inventory/ignite --ask-become-pass -e "ignite_action=destroy"
```
